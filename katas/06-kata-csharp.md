# C# Demo

<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: 3e75974e-742e-4114-9956-c5c8bd01997d
* title: Kata Demo
* docker_directory_path: /katas/custom-snippets/kata-csharp
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called SwapEnds that takes in an integer array "nums". Given an array of ints, swap the first and last elements in the array. Return the modified array. The array length will be at least 1.

For example:
```
SwapEnds([1, 2, 3, 4]) → [4, 2, 3, 1]
SwapEnds([1, 2, 3]) → [3, 2, 1]
SwapEnds([8, 6, 7, 9, 5]) → [5, 6, 7, 9, 8]
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
After you have made a reasonable effort, here is a solution walkthrough video:
<iframe title="vimeo-player" src="https://player.vimeo.com/video/560659894?h=a764374898" width="640" height="360" frameborder="0" allowfullscreen></iframe>
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
