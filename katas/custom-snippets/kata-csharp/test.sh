#!/bin/sh
submission_file=${1:-submission.txt}

cp "$submission_file" Kata/Exercise.cs

dotnet test --nologo > output.txt 2> /dev/null

# Search the results file for total number of tests.
number_of_tests=$(grep -P -o "Total tests: *\K\d+" output.txt|head -1)

# The tests didn't run and something else is wrong
if [ -z "$number_of_tests" ]
then
    echo "An error prevented your code from being tested."
    echo "Make sure: "
    echo "* your method has the correct method signature and return type"
    echo "* your method is contained in a namespace named Kata and a class named Exercise"
    echo "* all parentheses and brackets are in matching pairs"
    echo "* your method returns a value of the declared type"
    exit 1
fi

# Continue finding results
number_failed=$(grep -P -o "Failed: *\K\d+" output.txt|head -1)
number_passed=$(grep -P -o "Passed: *\K\d+" output.txt|head -1)


if [ -z "$number_passed" ]
then
    number_passed=0 # "Passed" line doesn't appear if there's no passing tests, so must set to 0 for (number_passed of number_of_tests)
fi


if [ $number_passed = $number_of_tests ]
then
    echo "All tests passed."
    exit 0
else
    cat output.txt
    exit 1
fi
