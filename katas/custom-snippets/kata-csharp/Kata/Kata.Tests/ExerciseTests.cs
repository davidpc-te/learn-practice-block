using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 4, 2, 3, 1 }, exercise.SwapEnds(new int[] { 1, 2, 3, 4 }));
            CollectionAssert.AreEqual(new int[] { 3, 2, 1 }, exercise.SwapEnds(new int[] { 1, 2, 3 }));
            CollectionAssert.AreEqual(new int[] { 5, 6, 7, 9, 8 }, exercise.SwapEnds(new int[] { 8, 6, 7, 9, 5 }));
        }
    }
}
