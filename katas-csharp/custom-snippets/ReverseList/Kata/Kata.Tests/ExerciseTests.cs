using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new List<string> { "green", "yellow", "blue", "green", "purple"  }, exercise.ReverseList(new List<string> { "purple", "green", "blue", "yellow", "green" }  ), "Testing with {'purple', 'green', 'blue', 'yellow', 'green' }");
            CollectionAssert.AreEqual(new List<string> { "way", "the", "all", "jingle", "bells", "jingle", "bells", "jingle" }, exercise.ReverseList(new List<string> { "jingle", "bells", "jingle", "bells", "jingle", "all", "the", "way" }), "Testing with {'jingle', 'bells', 'jingle', 'bells', 'jingle', 'all', 'the', 'way' }");
        }
    }
}
