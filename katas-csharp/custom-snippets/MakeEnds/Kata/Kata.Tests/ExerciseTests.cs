using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 1, 3 }, exercise.MakeEnds(new int[] { 1, 2, 3 }), "Testing with {1, 2, 3}");
            CollectionAssert.AreEqual(new int[] { 1, 4 }, exercise.MakeEnds(new int[] { 1, 2, 3, 4 }), "Testing with {1, 2, 3, 4}");
            CollectionAssert.AreEqual(new int[] { 7, 2 }, exercise.MakeEnds(new int[] { 7, 4, 6, 2 }), "Testing with {7, 4, 6, 2}");
        }
    }
}
