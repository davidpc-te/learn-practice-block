using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new string[] { "aa", "ab", "ac" }, exercise.List2Array(new List<string>{ "aa", "ab", "ac" }), "Testing with {'aa', 'ab', 'ac'}");
            CollectionAssert.AreEqual(new string[] { "as", "df", "jk" }, exercise.List2Array(new List<string> { "as", "df", "jk" }), "Testing with {'as', 'df', 'jk'}");
            CollectionAssert.AreEqual(new string[] { "aaa", "bbb", "ccc", "ddd" }, exercise.List2Array(new List<string> { "aaa", "bbb", "ccc", "ddd" }), "Testing with {'aaa', 'bbb', 'ccc', 'ddd'}");
        }
    }
}
