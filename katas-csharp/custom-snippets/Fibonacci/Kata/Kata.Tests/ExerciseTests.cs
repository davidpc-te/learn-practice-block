using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            int[] result = exercise.Fibonacci();

            Assert.AreEqual(1, result[0],"Testing 1 in position 1");
            Assert.AreEqual(1,result[1], "Testing 1 in position 2");
            Assert.AreEqual(2, result[2], "Testing 2 in position 3");
            Assert.AreEqual(3, result[3], "Testing 3 in position 4");
            Assert.AreEqual(5, result[4], "Testing 5 in position 5");
            Assert.AreEqual(8, result[5], "Testing 8 in position 6");
            Assert.AreEqual(13, result[6], "Testing 13 in position 7");
            Assert.AreEqual(1597, result[result.Length - 1], "Testing last in position is 1597");
        }
    }
}
