﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {

            //GreenTicket(1, 2, 3) → 0
            //GreenTicket(2, 2, 2) → 20
            //GreenTicket(1, 2, 1) → 10

            Assert.AreEqual(0, exercise.GreenTicket(1, 2, 3), "Testing with (1, 2, 3)");
            Assert.AreEqual(20, exercise.GreenTicket(2, 2, 2), "Testing with (2, 2, 2)");
            Assert.AreEqual(10, exercise.GreenTicket(1, 2, 1), "Testing with (1, 2, 1)");
        }
    }
}
