using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(9, exercise.SumOddsBetweenValues(0, 5), "Testing with 0, 5");
            Assert.AreEqual(29, exercise.SumOddsBetweenValues(28, 30), "Testing with 28, 30");
            Assert.AreEqual(0, exercise.SumOddsBetweenValues(18, 18), "Testing with 18, 18");
        }
    }
}
