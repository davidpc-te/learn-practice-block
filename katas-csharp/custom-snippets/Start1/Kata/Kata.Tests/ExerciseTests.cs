﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            //Start1({ 1, 2, 3}, { 1, 3}) → 2
            //Start1({ 7, 2, 3}, { 1}) → 1
            //Start1({ 2, 1}, { }) → 0

            Assert.AreEqual(2, exercise.Start1(new int[] { 1, 2, 3 }, new int[] { 1, 3 }), "Testing with [1, 2, 3] and [1, 2]");
            Assert.AreEqual(1, exercise.Start1(new int[] { 7, 2, 3 }, new int[] { 1 }), "Testing with [7, 2, 3] and [1]");
            Assert.AreEqual(0, exercise.Start1(new int[] { 2, 1 }, new int[0] ), "Testing with [2, 1] and []");
        }
    }
}
