using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("lololo", exercise.ExtraEnd("Hello"), "Testing with 'Hello'");
            Assert.AreEqual("ababab", exercise.ExtraEnd("ab"), "Testing with 'ab'");
            Assert.AreEqual("HiHiHi", exercise.ExtraEnd("Hi"), "Testing with 'Hi'");
        }
    }
}
