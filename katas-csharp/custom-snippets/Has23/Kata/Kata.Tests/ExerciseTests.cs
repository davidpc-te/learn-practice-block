using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.Has23(new int[] { 2, 5 }), "Testing with {2, 5}");
            Assert.AreEqual(true, exercise.Has23(new int[] { 4, 3 }), "Testing with {4, 3}");
            Assert.AreEqual(false, exercise.Has23(new int[] { 4, 5 }), "Testing with {4, 5}");
        }
    }
}
