using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.FoundIntTwice(new List<int> { 5, 7, 9, 5, 11}, 5), "Testing with {5, 7, 9, 5, 11} & 5");
            Assert.AreEqual(false, exercise.FoundIntTwice(new List<int> { 6, 8, 10, 11, 13 }, 8), "Testing with {6, 8, 10, 11, 13 } & 8");
            Assert.AreEqual(true, exercise.FoundIntTwice(new List<int> { 9, 23, 44, 2, 88, 44 }, 44), "Testing with { 9, 23, 44, 2, 88, 44 } & 44");
        }
    }
}
