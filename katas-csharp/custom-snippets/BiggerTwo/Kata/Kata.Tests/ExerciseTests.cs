using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 3, 4 }, exercise.BiggerTwo(new int[] { 1, 2 }, new int[] { 3, 4 }), "Testing with {1, 2}, {3, 4}");
            CollectionAssert.AreEqual(new int[] { 3, 4 }, exercise.BiggerTwo(new int[] { 3, 4 }, new int[] { 1, 2 }), "Testing with {3, 4}, {1, 2}");
            CollectionAssert.AreEqual(new int[] { 3, 1 }, exercise.BiggerTwo(new int[] { 3, 1 }, new int[] { 2, 2 }), "Testing with {3, 1}, {2, 2}");
        }
    }
}
