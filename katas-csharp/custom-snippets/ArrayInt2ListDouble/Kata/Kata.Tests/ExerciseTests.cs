using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new List<double> { 2.5, 4.0, 5.5, 100, 48.5 }, exercise.ArrayInt2ListDouble(new int[] { 5, 8, 11, 200, 97 }), "Testing with {5, 8, 11, 200, 97}");
            CollectionAssert.AreEqual(new List<double> { 372.5, 11.5, 22, 4508.5, 3 }, exercise.ArrayInt2ListDouble(new int[] { 745, 23, 44, 9017, 6 }), "Testing with {745, 23, 44, 9017, 6}");
            CollectionAssert.AreEqual(new List<double> { 42, 49.5, 1642.5, 6.5, 438.5 }, exercise.ArrayInt2ListDouble(new int[] { 84, 99, 3285, 13, 877 }), "Testing with {84, 99, 3285, 13, 877}");
        }
    }
}
