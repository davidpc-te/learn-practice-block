using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(21, exercise.Blackjack(19, 21 ), "Testing with {19, 21}");
            Assert.AreEqual(21, exercise.Blackjack( 21, 19 ), "Testing with {21, 19}");
            Assert.AreEqual(19, exercise.Blackjack( 19, 22 ), "Testing with {19, 22}");
        }
    }
}
