using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("He", exercise.FirstTwo("Hello"), "Testing with 'Hello'");
            Assert.AreEqual("ab", exercise.FirstTwo("abcdefg"), "Testing with 'abcdefg'");
            Assert.AreEqual("ab", exercise.FirstTwo("ab"), "Testing with 'ab'");
        }
    }
}
