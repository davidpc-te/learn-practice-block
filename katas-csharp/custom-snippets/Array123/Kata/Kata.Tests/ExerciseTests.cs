using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.Array123(new int[] { 1, 1, 2, 3, 1 }), "Testing with {1, 1, 2, 3, 1}");
            Assert.AreEqual(false, exercise.Array123(new int[] { 1, 1, 2, 4, 3 }), "Testing with {1, 1, 2, 4, 3}");
            Assert.AreEqual(true, exercise.Array123(new int[] { 1, 1, 2, 1, 2, 3 }), "Testing with {1, 1, 2, 1, 2, 3}");
        }
    }
}
