using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 2, 3 }, exercise.MakeMiddle(new int[] { 1, 2, 3, 4 }), "Testing with {1, 2, 3, 4 }");
            CollectionAssert.AreEqual(new int[] { 2, 3 }, exercise.MakeMiddle(new int[] { 7, 1, 2, 3, 4, 9 }), "Testing with {7, 1, 2, 3, 4, 9}");
            CollectionAssert.AreEqual(new int[] { 1, 2 }, exercise.MakeMiddle(new int[] { 1, 2 }), "Testing with {1, 2}}");
        }
    }
}
