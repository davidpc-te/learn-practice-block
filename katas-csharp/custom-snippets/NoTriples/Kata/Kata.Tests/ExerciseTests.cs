﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {

            //NoTriples({ 1, 1, 2, 2, 1}) → true
            //NoTriples({ 1, 1, 2, 2, 2, 1}) → false
            //NoTriples({ 1, 1, 1, 2, 2, 2, 1}) → false

            Assert.AreEqual(true, exercise.NoTriples(new int[] { 1, 1, 2, 2, 1 }), "Testing with {1, 1, 2, 2, 1}");
            Assert.AreEqual(false, exercise.NoTriples(new int[] { 1, 1, 2, 2, 2, 1 }), "Testing with {1, 1, 2, 2, 2, 1}");
            Assert.AreEqual(false, exercise.NoTriples(new int[] { 1, 1, 1, 2, 2, 2, 1 }), "Testing with {1, 1, 1, 2, 2, 2, 1}");
        }
    }
}
