using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("hiHellohi", exercise.ComboString("Hello", "hi"), "Testing with 'Hello', 'hi'");
            Assert.AreEqual("hiHellohi", exercise.ComboString("hi", "Hello"), "Testing with 'hi', 'Hello'");
            Assert.AreEqual("baaab", exercise.ComboString("aaa", "b"), "Testing with 'aaa', 'b'");
        }
    }
}
