using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.HasBad("badxx"), "Testing with 'badxx'");
            Assert.AreEqual(true, exercise.HasBad("xbadxx"), "Testing with 'xbadxx'");
            Assert.AreEqual(false, exercise.HasBad("xxbadxx"), "Testing with 'xxbadxx'");
        }
    }
}
