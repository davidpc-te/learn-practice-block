using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("Woo", exercise.FirstHalf("WooHoo"), "Testing with 'WooHoo'");
            Assert.AreEqual("Hello", exercise.FirstHalf("HelloThere"), "Testing with 'HelloThere'");
            Assert.AreEqual("abc", exercise.FirstHalf("abcdef"), "Testing with 'abcdef'");
        }
    }
}
