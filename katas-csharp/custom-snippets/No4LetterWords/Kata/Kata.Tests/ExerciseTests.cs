using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new List<string> { "Train", "Car" }, exercise.No4LetterWords(new string[] { "Train", "Boat", "Car" }), "Testing with { 'Train', 'Boat', 'Car' }");
            CollectionAssert.AreEqual(new List<string> { "Red", "White" }, exercise.No4LetterWords(new string[] { "Red", "White", "Blue" }), "Testing with { 'Red', 'White', 'Blue' }");
        }
    }
}
