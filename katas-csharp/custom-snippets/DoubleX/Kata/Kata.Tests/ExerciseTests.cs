using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.DoubleX("axxbb"), "Testing with 'axxbb'");
            Assert.AreEqual(false, exercise.DoubleX("axaxax"), "Testing with 'axaxax'");
            Assert.AreEqual(true, exercise.DoubleX("xxxxx"), "Testing with 'xxxxx'");
        }
    }
}
