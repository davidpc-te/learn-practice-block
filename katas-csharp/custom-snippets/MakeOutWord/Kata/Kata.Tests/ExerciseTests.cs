using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("<<Yay>>", exercise.MakeOutWord("<<>>", "Yay"), "Testing with '<<>>' & 'Yay'");
            Assert.AreEqual("<<WooHoo>>", exercise.MakeOutWord("<<>>", "WooHoo"), "Testing with '<<>>', 'WooHoo'");
            Assert.AreEqual("[[word]]", exercise.MakeOutWord("[[]]", "word"), "Testing with '[[]]', 'word'");
        }
    }
}
