using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.LessBy10(1, 7, 11 ), "Testing with ( 1, 7, 11)");
            Assert.AreEqual(false, exercise.LessBy10( 1, 7, 10 ), "Testing with (1, 7, 10)");
            Assert.AreEqual(true, exercise.LessBy10(11, 1, 7 ), "Testing with (11, 1, 7)");
        }
    }
}
