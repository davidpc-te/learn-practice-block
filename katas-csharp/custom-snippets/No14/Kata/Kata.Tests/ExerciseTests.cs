using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.No14(new int[] { 7, 2, 3 }), "Testing with {7, 2, 3}");
            Assert.AreEqual(false, exercise.No14(new int[] { 1, 2, 3, 4 }), "Testing with {1, 2, 3, 4}");
            Assert.AreEqual(false, exercise.No14(new int[] { 2, 3, 4 }), "Testing with { 2, 3, 4}");
        }
    }
}
