using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.EvenlySpaced(new int[] { 2, 4, 6 }), "Testing with {2, 4, 6}");
            Assert.AreEqual(true, exercise.EvenlySpaced(new int[] { 4, 6, 2 }), "Testing with {4, 6, 2}");
            Assert.AreEqual(false, exercise.EvenlySpaced(new int[] { 4, 6, 3 }), "Testing with {4, 6, 3}");
        }
    }
}
