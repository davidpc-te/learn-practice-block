using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.EndsLy("oddly"), "Testing with 'oddly'");
            Assert.AreEqual(false, exercise.EndsLy("y"), "Testing with 'y'");
            Assert.AreEqual(false, exercise.EndsLy("oddy"), "Testing with 'oddy'");
        }
    }
}
