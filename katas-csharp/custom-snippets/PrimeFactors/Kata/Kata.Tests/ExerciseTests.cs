using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 2, 3 }, exercise.PrimeFactors(6), "Testing with 6");
            CollectionAssert.AreEqual(new int[] { 2, 2, 7 }, exercise.PrimeFactors(28), "Testing with 28");
            CollectionAssert.AreEqual(new int[] { 23, 29 }, exercise.PrimeFactors(667), "Testing with 667");
        }
    }
}
