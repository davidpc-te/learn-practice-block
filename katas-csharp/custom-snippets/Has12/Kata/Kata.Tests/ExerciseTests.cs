using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.Has12(new int[] { 1, 3, 2 }), "Testing with {1, 3, 2}");
            Assert.AreEqual(false, exercise.Has12(new int[] { 3, 2, 1 }), "Testing with {3, 2, 1}");
            Assert.AreEqual(true, exercise.Has12(new int[] { 3, 1, 4, 5, 2 }), "Testing with {3, 1, 4, 5, 2}");
        }
    }
}
