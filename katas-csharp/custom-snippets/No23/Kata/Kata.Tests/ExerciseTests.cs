﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {

            //No23({ 4, 5}) → true
            //No23({ 4, 2}) → false
            //No23({ 3, 5}) → false

            Assert.AreEqual(true, exercise.No23(new int[] { 4, 5 }), "Testing with { 4, 5 }");
            Assert.AreEqual(false, exercise.No23(new int[] { 4, 2 }), "Testing with { 4, 2 }");
            Assert.AreEqual(false, exercise.No23(new int[] { 3, 5 }), "Testing with { 3, 5 }");

        }
    }
}
