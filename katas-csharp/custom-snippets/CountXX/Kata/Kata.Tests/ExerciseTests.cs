using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(1, exercise.CountXX("abcxx"), "Testing with 'abcxx'");
            Assert.AreEqual(2, exercise.CountXX("xxx"), "Testing with 'xxx'");
            Assert.AreEqual(3, exercise.CountXX("xxxx"), "Testing with 'xxxx'");
        }
    }
}
