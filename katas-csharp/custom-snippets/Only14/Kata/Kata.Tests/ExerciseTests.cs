﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            //Only14({ 1, 4, 1, 4}) → true
            //Only14({ 1, 4, 2, 4}) → false
            //Only14({ 1, 1}) → true

            Assert.AreEqual(true, exercise.Only14(new int[] { 1, 4, 1, 4 }), "Testing with {1, 4, 1, 4}");
            Assert.AreEqual(false, exercise.Only14(new int[] { 1, 4, 2, 4 }), "Testing with {1, 4, 2, 4}");
            Assert.AreEqual(true, exercise.Only14(new int[] { 1, 1 }), "Testing with {1, 1}");
        }
    }
}
