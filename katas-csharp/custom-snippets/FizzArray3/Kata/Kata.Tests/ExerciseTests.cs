﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();


        //FizzArray3(5, 10) → {5, 6, 7, 8, 9}
        //FizzArray3(11, 12) → {11}
        //FizzArray3(3, 3) → {}

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 5, 6, 7, 8, 9 }, exercise.FizzArray3(5, 10), "Testing with (5, 10)");
            CollectionAssert.AreEqual(new int[] { 11}, exercise.FizzArray3(11, 12), "Testing with (11, 12)");
            CollectionAssert.AreEqual(new int[0], exercise.FizzArray3(3, 3), "Testing with (3, 3)");
        }
    }
}
