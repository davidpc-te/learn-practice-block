using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 1, 2, 0 }, exercise.Fix23(new int[] { 1, 2, 3 }), "Testing with {1, 2, 3}");
            CollectionAssert.AreEqual(new int[] { 2, 0, 5 }, exercise.Fix23(new int[] { 2, 3, 5 }), "Testing with {2, 3, 5}");
            CollectionAssert.AreEqual(new int[] { 1, 2, 1 }, exercise.Fix23(new int[] { 1, 2, 1 }), "Testing with {1, 2, 1}");
        }
    }
}
