# Find Largest
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5d3ca09-b056-48b7-aeb2-42c94d4535d8
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/FindLargest
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer method called `FindLargest` that takes in a List of integers called `nums`. Return the largest value in `nums`.

For example:
```
FindLargest( [11, 200, 43, 84, 9917, 4321, 1, 33333, 8997] ) →  33333
FindLargest( [987, 1234, 9381, 731, 43718, 8932] ) →  43718
FindLargest( [34070, 1380, 81238, 7782, 234, 64362, 627] ) →  81238.
```
##### !end-question

##### !placeholder


```
using System;
using System.Collections.Generic;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int FindLargest(List<int> nums)
{
    return 0;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
