# Make Middle
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5d6bc5d-fbff-4531-84d6-ee0e7e72eb00
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/MakeMiddle
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `MakeMiddle` that takes in an integer array `nums`. Return a new array length 2 containing the middle two elements from `nums`. You can assume the length of `nums` is even and 2 or more.

For example:
```
MakeMiddle({1, 2, 3, 4}) → {2, 3}
MakeMiddle({7, 1, 2, 3, 4, 9}) → {2, 3}
MakeMiddle({1, 2}) → {1, 2}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] MakeMiddle(int[] nums)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
