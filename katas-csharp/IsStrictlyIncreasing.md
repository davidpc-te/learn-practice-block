# Is Strictly Increasing
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5151e4a-fc02-4cca-9d49-b97376ee3182
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/IsStrictlyIncreasing
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question
 
Create a boolean method called `IsStrictlyIncreasing` that takes in an integer array called `nums`. Return true if the values in the array are strictly increasing. Return false otherwise.

For example:
```
IsStrictlyIncreasing({5,7,8,10}) → true
IsStrictlyIncreasing({5,7,7,10}) → false
IsStrictlyIncreasing({-5,-3,0,17}) → true
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public bool IsStrictlyIncreasing(int[] nums)
{
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
