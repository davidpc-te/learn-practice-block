# Sum Odds Between Values
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c54211aa-4797-4646-8ad6-eae9e475745d
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/SumOddsBetweenValues
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer method called `SumOddsBetweenValues` that takes in two integers `start` and `end`. Return the sum of the odd integers between `start` and `end` inclusive. You can assume `end` isn't less than `start` (but they may be equal).


For example:
```
SumOddsBetweenValues(0, 5) → 9
SumOddsBetweenValues(28,30) → 29
SumOddsBetweenValues(18, 18) → 0

```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int SumOddsBetweenValues(int start, int end)
{
    return 0;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
