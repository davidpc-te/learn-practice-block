# No 4 Letter Words
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5811c24-0207-4913-8859-12f3c365cc01
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/No4LetterWords
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a string List method called `No4LetterWords` that takes in an array of strings called `strings`. Return a List containing the elements of `strings` in the same order except for any that contain exactly 4 characters.

For example:
```
No4LetterWords( {"Train", "Boat", "Car"} ) →   ["Train", "Car"]
No4LetterWords( {"Red", "White", "Blue"} ) →   ["Red", "White"]
```
##### !end-question

##### !placeholder


```
using System;
using System.Collections.Generic;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public List<string> No4LetterWords(string[] strings)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
