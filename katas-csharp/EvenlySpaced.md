# Evenly Spaced
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5150d2e-fcdf-4816-9d1f-d3d6ec4498b6
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/EvenlySpaced
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a boolean method called `EvenlySpaced` that takes in three integers, `a`, `b`, and `c`.  Return true if `a`, `b`, and `c` are evenly spaced, so the difference between the lowest value and the middle value is the same as the difference between the middle value and the highest value. Otherwise, return false.

For example:
```
EvenlySpaced(2, 4, 6) → true
EvenlySpaced(4, 6, 2) → true
EvenlySpaced(4, 6, 3) → false
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public bool EvenlySpaced(int a, int b, int c)
{
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
