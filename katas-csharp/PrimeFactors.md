# Prime Factors
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c51a31bd-222e-49db-b7e9-76dd9004a84e
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/PrimeFactors
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `PrimeFactors` that takes in an integer `n`. Return an integer array of the [prime factors] of `n`(https://www.mathsisfun.com/definitions/prime-factor.html). Prime factors are the numbers you can multiply to get `n` that you can't break down into any smaller factors. You can assume the input is greater than 1.

For example:
```
PrimeFactors(6) → {2, 3}
PrimeFactors(28) → {2, 2, 7}
PrimeFactors(667) → {23, 29}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] PrimeFactors(int n)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
