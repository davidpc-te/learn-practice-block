# No Triples
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c59f62a4-2b37-4cb5-ad5c-2f47f76a2489
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/NoTriples
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a boolean method called `NoTriples` that takes in an integer array `nums`.  A triple is a value appearing 3 times in a row. Return true if there are no triples in `nums`. Otherwise return false.

For example:
```
NoTriples({1, 1, 2, 2, 1}) → true
NoTriples({1, 1, 2, 2, 2, 1}) → false
NoTriples({1, 1, 1, 2, 2, 2, 1}) → false
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public bool NoTriples(int[] nums)
{
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
