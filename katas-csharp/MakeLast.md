# Make Last
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5666c5e-0afa-47e8-8cb4-9ad09991f2f4
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/MakeLast
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `MakeLast` that takes in an integer array `nums`. Return a new array with double the length of `nums` where its last element is the same as `nums`, and all the other elements are 0. You can assume `nums` is length 1 or more.

For example:
```
MakeLast({4, 5, 6}) → {0, 0, 0, 0, 0, 6}
MakeLast({1, 2}) → {0, 0, 0, 2}
MakeLast({3}) → {0, 3}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] MakeLast(int[] nums)
{
    return new int[]{};
}
```
##### !end-hint

##### !hint
By default, a new int array contains all 0's.
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
