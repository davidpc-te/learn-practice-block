# Array To List
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5b3c96a-e17a-408a-8e56-9ac7573f28e6
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/Array2List
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a string List method called `Array2List` that takes in a string array called `strings`. Return a List containing the elements of `strings` in the same order. Avoid using a standard library method that does the conversion for you. 
For example:
```
Array2List( {"Apple", "Orange", "Banana"} ) →   ["Apple", "Orange", "Banana"]
Array2List( {"Red", "Orange", "Yellow"} ) →   ["Red", "Orange", "Yellow"]
Array2List( {"Left", "Right", "Forward", "Back"} ) →   ["Left", "Right", "Forward", "Back"] 
```
##### !end-question

##### !placeholder


```
using System;
using System.Collections.Generic;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public List<string> Array2List(string[] strings)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
