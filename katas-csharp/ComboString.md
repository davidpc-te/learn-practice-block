# Combo String
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5c61223-b3f7-4f41-9f19-cdb24166d489
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/ComboString
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a string method called `ComboString` that takes in two strings, `a` and `b`.
Return a string of the form short+long+short, where short is the shorter of `a` and `b` and long is the longer of the two. You can assume the strings are different lengths, but they may be empty (length 0).

For example:
```
ComboString("Hello", "hi") → "hiHellohi"
ComboString("hi", "Hello") → "hiHellohi"
ComboString("aaa", "b") → "baaab"
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public string ComboString(string a, string b)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
