# Sum Odds Between Values
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: ad4211aa-4797-4646-8ad6-eae9e475745d
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/SumOddsBetweenValues
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `sumOddsBetweenValues` that takes in two integers `start` and `end`. Return the sum of the odd integers between `start` and `end` inclusive. You can assume `end` isn't less than `start` (but they may be equal).


For example:
```
sumOddsBetweenValues(0, 5) → 9
sumOddsBetweenValues(28,30) → 29
sumOddsBetweenValues(18, 18) → 0

```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int sumOddsBetweenValues(int start, int end) {
    return 0;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
