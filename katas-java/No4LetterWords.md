# No 4 Letter Words
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 4e811c24-0207-4913-8859-12f3c365cc01
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/No4LetterWords
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `no4LetterWords` that takes in an array of strings called `strings`. Return a List containing the elements of `strings` in the same order except for any that contain exactly 4 characters.

For example:
```
no4LetterWords( {"Train", "Boat", "Car"} ) →   ["Train", "Car"]
no4LetterWords( {"Red", "White", "Blue"} ) →   ["Red", "White"]
```
##### !end-question

##### !placeholder


```
import java.util.List;
import java.util.ArrayList;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public List<String> no4LetterWords(String[] strings) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
