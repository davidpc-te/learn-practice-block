# Distinct Values
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: bddc9ab8-b8a1-47e8-8dce-84569c554370
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/DistinctValues
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `distinctValues` that takes in a List of strings called `strings`. Return a List that contains the distinct values in `strings`.

For example:
```
distinctValues( ["red", "yellow", "green", "yellow", "blue", "green", "purple"] ) →  ["red", "yellow", "green", "blue", "purple"]
distinctValues( ["jingle", "bells", "jingle", "bells", "jingle", "all", "the", "way"] ) →  ["jingle", "bells", "all", "the", "way"]
```
##### !end-question

##### !placeholder


```
import java.util.List;
import java.util.ArrayList;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public List<String> distinctValues(List<String> strings) {
     return null;
}
```
##### !end-hint

##### !hint
Think `HashSet`.
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
