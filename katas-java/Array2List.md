# Array To List
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 13b3c96a-e17a-408a-8e56-9ac7573f28e6
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/Array2List
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `array2List` that takes in a string array called `strings`. Return a List containing the elements of `strings` in the same order. Avoid using a standard library method that does the conversion for you. 
For example:
```
array2List( {"Apple", "Orange", "Banana"} ) →   ["Apple", "Orange", "Banana"]
array2List( {"Red", "Orange", "Yellow"} ) →   ["Red", "Orange", "Yellow"]
array2List( {"Left", "Right", "Forward", "Back"} ) →   ["Left", "Right", "Forward", "Back"] 
```
##### !end-question

##### !placeholder


```
import java.util.List;
import java.util.ArrayList;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public List<String> array2List(String[] strings) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
