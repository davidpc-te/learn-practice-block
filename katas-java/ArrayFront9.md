# Array Front 9
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 0b5c72ae-1819-44a6-9255-0cc170c6735f
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/ArrayFront9
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `arrayFront9` that takes in an integer array called `nums`. Return true if one of the first 4 elements in `nums` is a 9. The length of `nums` may be less than 4.

For example:
```
arrayFront9({1, 2, 9, 3, 4}) → true
arrayFront9({1, 2, 3, 4, 9}) → false
arrayFront9({9, 0}) → true
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public boolean arrayFront9(int[] nums) {
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
