# Double 2 3
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: d5f18615-20c0-41f2-bba5-d33a50b270e0
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/Double23
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `double23` that takes in an integer array called `nums`. Return true if `nums` contains 2 twice or 3 twice. You can assume the length of `nums` is 0, 1, or 2.

For example:
```
double23({2, 2}) → true
double23({3, 3}) → true
double23({2, 3}) → false
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public boolean double23(int[] nums) {
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
