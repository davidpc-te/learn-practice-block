# Find Largest
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 59d3ca09-b056-48b7-aeb2-42c94d4535d8
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/FindLargest
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `findLargest` that takes in a List of integers called `nums`. Return the largest value in `nums`.

For example:
```
findLargest( [11, 200, 43, 84, 9917, 4321, 1, 33333, 8997] ) →  33333
findLargest( [987, 1234, 9381, 731, 43718, 8932] ) →  43718
findLargest( [34070, 1380, 81238, 7782, 234, 64362, 627] ) →  64362.
```
##### !end-question

##### !placeholder


```
import java.util.List;
import java.util.ArrayList;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int findLargest(List<Integer> nums) {
    return 0;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
