# Fibonacci
<!--New for V3 -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 9aeb1e32-5847-4009-92d2-db720318fa7a
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/Fibonacci
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `fibonacci` with no parameters. In a Fibonacci sequence, every number after the first two is the sum of the two preceding ones. Return an array of integers containing the Fibonacci sequence of 0, 1, 1, 2, 3, and so on for the values less than 2000.

For example:
```
fibonacci() → {1, 1, 2, 3, 5, 8, 13, ... 987, 1597}
```

##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int[] fibonacci() {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
