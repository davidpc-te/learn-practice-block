import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 3, 2}", true, kata.has12(new int[] {1, 3, 2}));
        assertEquals("Testing with {3, 2, 1}", false, kata.has12(new int[] {3, 2, 1}));
        assertEquals("Testing with {3, 1, 4, 5, 2}", true, kata.has12(new int[] {3, 1, 4, 5, 2}));
    }


}
