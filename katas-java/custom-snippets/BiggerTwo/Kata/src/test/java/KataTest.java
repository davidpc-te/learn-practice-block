import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with {1, 2} and {3, 4}", new int[] {3, 4}, kata.biggerTwo(new int[] {1, 2}, new int[] {3, 4}));
        assertArrayEquals("Testing with {1, 2} and {3, 4}", new int[] {3, 4}, kata.biggerTwo(new int[] {1, 2}, new int[] {3, 4}));
        assertArrayEquals("Testing with {1, 2} and {3, 4}", new int[] {3, 4}, kata.biggerTwo(new int[] {1, 2}, new int[] {3, 4}));
    }


}
