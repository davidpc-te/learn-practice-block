import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with [5, 8, 11, 200, 97]",
                Arrays.asList(2.5, 4.0, 5.5, 100.0, 48.5),
                kata.arrayInt2ListDouble(new int[] {5, 8, 11, 200, 97}));

        assertEquals("Testing with [745, 23, 44, 9017, 6]",
                Arrays.asList(372.5, 11.5, 22.0, 4508.5, 3.0),
                kata.arrayInt2ListDouble(new int[] {745, 23, 44, 9017, 6}));

        assertEquals("Testing with[84, 99, 3285, 13, 877]",
                Arrays.asList(42.0, 49.5, 1642.5, 6.5, 438.5),
                kata.arrayInt2ListDouble(new int[] {84, 99, 3285, 13, 877}));
    }


}
