import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"Chocolate\", 2", "ChoCho", kata.frontTimes("Chocolate", 2));
        assertEquals("Testing with \"Chocolate\", 3", "ChoChoCho", kata.frontTimes("Chocolate", 3));
        assertEquals("Testing with \"Ab\", 3", "AbAbAb", kata.frontTimes("Ab", 3));
    }


}
