import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("firstHalf with \"WooHoo\"", "Woo", kata.firstHalf("WooHoo"));
        assertEquals("Testing with \"HelloThere\"", "Hello", kata.firstHalf("HelloThere"));
        assertEquals("Testing with \"abcdef\"", "abc", kata.firstHalf("abcdef"));
    }


}
