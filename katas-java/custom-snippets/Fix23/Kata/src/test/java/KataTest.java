import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with {1, 2, 3}", new int[] {1, 2, 0}, kata.fix23(new int[] {1, 2, 3}));
        assertArrayEquals("Testing with {2, 3, 5}", new int[] {2, 0, 5}, kata.fix23(new int[] {2, 3, 5}));
        assertArrayEquals("Testing with {1, 2, 1}", new int[] {1, 2, 1}, kata.fix23(new int[] {1, 2, 1}));
    }


}
