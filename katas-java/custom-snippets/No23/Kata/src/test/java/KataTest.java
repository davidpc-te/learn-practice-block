import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {4, 5}", true, kata.no23(new int[] {4, 5}));
        assertEquals("Testing with {4, 2}", false, kata.no23(new int[] {4, 2}));
        assertEquals("Testing with {3, 5}", false, kata.no23(new int[] {3, 5}));


    }


}
