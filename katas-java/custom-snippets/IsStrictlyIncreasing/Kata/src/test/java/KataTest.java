import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {5,7,8,10}", true, kata.isStrictlyIncreasing(new int [] {5,7,8,10}));
        assertEquals("Testing with {5,7,7,10}", false, kata.isStrictlyIncreasing(new int [] {5,7,7,10}));
        assertEquals("Testing with {-5,-3,0,17}", true, kata.isStrictlyIncreasing(new int [] {-5,-3,0,17}));
    }


}
