import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {11, 200, 43, 84, 9917, 4321, 1, 33333, 8997}",
                33333, kata.findLargest(Arrays.asList(11, 200, 43, 84, 9917, 4321, 1, 33333, 8997)));

        assertEquals("Testing with {987, 1234, 9381, 731, 43718, 8932}",
                43718, kata.findLargest(Arrays.asList(987, 1234, 9381, 731, 43718, 8932)));

        assertEquals("Testing with {34070, 1380, 81238, 7782, 234, 64362, 627}",
                81238, kata.findLargest(Arrays.asList(34070, 1380, 81238, 7782, 234, 64362, 627)));
    }


}
