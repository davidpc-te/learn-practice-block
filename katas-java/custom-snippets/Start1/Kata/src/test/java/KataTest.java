import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 2, 3}, {1, 3}", 2, kata.start1(new int[] {1, 2, 3}, new int[] {1, 3}));
        assertEquals("Testing with {7, 2, 3}, {1}", 1, kata.start1(new int[] {7, 2, 3}, new int[] {1}));
        assertEquals("Testing with {2, 1}, {}", 0, kata.start1(new int[] {2, 1}, new int[] {}));
    }


}
