import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with (19, 21)", 21, kata.blackjack(19, 21));
        assertEquals("Testing with (21, 19)", 21, kata.blackjack(21, 19));
        assertEquals("Testing with (19, 22)", 19, kata.blackjack(19, 22));

    }


}
