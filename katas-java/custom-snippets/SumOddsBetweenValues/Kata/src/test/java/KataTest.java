import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with (0, 5)", 9, kata.sumOddsBetweenValues(0, 5));
        assertEquals("Testing with (28, 30)", 29, kata.sumOddsBetweenValues(28,30));
        assertEquals("Testing with (18, 18)", 0, kata.sumOddsBetweenValues(18, 18));
    }


}
