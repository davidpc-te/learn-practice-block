import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {2, 5}", true, kata.has23(new int[] {1, 3, 2}));
        assertEquals("Testing with {4, 3}", true, kata.has23(new int[] {4, 3}));
        assertEquals("Testing with {4, 5}", false, kata.has23(new int[] {4, 5}));
    }


}
