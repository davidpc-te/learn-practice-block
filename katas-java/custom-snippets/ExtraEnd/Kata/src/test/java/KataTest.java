import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"Hello\"", "lololo", kata.extraEnd("Hello"));
        assertEquals("Testing with \"ab\"", "ababab", kata.extraEnd("ab"));
        assertEquals("Testing with \"Hi\"", "HiHiHi", kata.extraEnd("Hi"));
    }


}
