import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"badxx\"", true, kata.hasBad("badxx"));
        assertEquals("Testing with \"xbadxx\"", true, kata.hasBad("xbadxx"));
        assertEquals("Testing with \"xxbadxx\"", false, kata.hasBad("xxbadxx"));
    }


}
