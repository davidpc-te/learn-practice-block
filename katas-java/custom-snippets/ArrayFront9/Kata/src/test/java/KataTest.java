import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 2, 9, 3, 4}", true, kata.arrayFront9(new int[] {1, 2, 9, 3, 4}));
        assertEquals("Testing with {1, 2, 3, 4, 9}", false, kata.arrayFront9(new int[] {1, 2, 3, 4, 9}));
        assertEquals("Testing with {9, 0}", true, kata.arrayFront9(new int[] {9, 0}));
    }


}
