import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {\"purple\", \"green\", \"blue\", \"yellow\", \"green\"}",
            Arrays.asList("green", "yellow", "blue", "green", "purple"),
            kata.reverseList(Arrays.asList("purple", "green", "blue", "yellow", "green")));

        assertEquals("Testing with {\"jingle\", \"bells\", \"jingle\", \"bells\", \"jingle\", \"all\", \"the\", \"way\"}",
                Arrays.asList("way", "the", "all", "jingle", "bells", "jingle", "bells", "jingle"),
                kata.reverseList(Arrays.asList("jingle", "bells", "jingle", "bells", "jingle", "all", "the", "way")));
    }


}
