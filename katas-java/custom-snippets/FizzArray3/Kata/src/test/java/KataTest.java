import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with 5 and 10", new int[] { 5, 6, 7, 8, 9}, kata.fizzArray3(5,10));
        assertArrayEquals("Testing with 11 and 12", new int[] {11}, kata.fizzArray3(11, 12));
       assertArrayEquals("Testing with 3 and 3", new int[] {}, kata.fizzArray3(3, 3));
    }


}
