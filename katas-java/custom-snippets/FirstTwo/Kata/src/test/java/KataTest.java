import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"Hello\"", "He", kata.firstTwo("Hello"));
        assertEquals("Testing with \"abcdefg\"", "ab", kata.firstTwo("abcdefg"));
        assertEquals("Testing with \"ab\"", "ab", kata.firstTwo("ab"));
    }


}
