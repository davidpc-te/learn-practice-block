import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"oddly\"", true, kata.endsLy("oddly"));
        assertEquals("Testing with \"y\"", false, kata.endsLy("y"));
        assertEquals("Testing with \"oddy\"", false, kata.endsLy("oddy"));
    }


}
