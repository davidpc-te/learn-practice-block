import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with (4)", new int[] {0, 1, 2, 3}, kata.fizzArray(4));
        assertArrayEquals("Testing with (1)", new int[] {0}, kata.fizzArray(1));
        assertArrayEquals("Testing with (0)", new int[] {}, kata.fizzArray(0));
    }


}
