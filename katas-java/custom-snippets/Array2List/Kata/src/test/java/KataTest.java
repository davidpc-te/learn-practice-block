import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with [\"Apple\", \"Orange\", \"Banana\"]",
                Arrays.asList("Apple", "Orange", "Banana"),
                kata.array2List(new String[] {"Apple", "Orange", "Banana"}));

        assertEquals("Testing with [\"Red\", \"Orange\", \"Yellow\"]",
                Arrays.asList("Red", "Orange", "Yellow"),
                kata.array2List(new String[] {"Red", "Orange", "Yellow"} ));

        assertEquals("Testing with [\"Left\", \"Right\", \"Forward\", \"Back\"]",
                Arrays.asList("Left", "Right", "Forward", "Back"),
                kata.array2List(new String[] {"Left", "Right", "Forward", "Back"}));
    }


}
