import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {5, 7, 9, 5, 11}, 5",
                true, kata.foundIntTwice(Arrays.asList(5, 7, 9, 5, 11), 5));

        assertEquals("Testing with {6, 8, 10, 11, 13}, 8",
                false, kata.foundIntTwice(Arrays.asList(6, 8, 10, 11, 13), 8));

        assertEquals("Testing with {9, 23, 44, 2, 88, 44}, 44",
                true, kata.foundIntTwice(Arrays.asList(9, 23, 44, 2, 88, 44), 44));
    }


}
