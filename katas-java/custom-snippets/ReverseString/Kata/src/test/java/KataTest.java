import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with ('Hello!')", "!olleH", kata.reverseString("Hello!"));
        assertEquals("Testing with ('Kata')", "ataK", kata.reverseString("Kata"));
        assertEquals("Testing with ('')", "", kata.reverseString(""));
    }


}
