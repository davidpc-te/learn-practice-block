import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with (2, 4, 6)", true, kata.evenlySpaced(2, 4, 6));
        assertEquals("Testing with (4, 6, 2)", true, kata.evenlySpaced(4, 6, 2));
        assertEquals("Testing with (4, 6, 3)", false, kata.evenlySpaced(4, 6, 3));
    }


}
