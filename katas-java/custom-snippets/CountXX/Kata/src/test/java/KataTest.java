import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"abcxx\"", 1, kata.countXX("abcxx"));
        assertEquals("Testing with \"xxx\"", 2, kata.countXX("xxx"));
        assertEquals("Testing with \"xxxx\"", 3, kata.countXX("xxxx"));
    }


}
