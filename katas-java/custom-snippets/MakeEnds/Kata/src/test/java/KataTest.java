import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with {1, 2, 3}", new int[] {1, 3}, kata.makeEnds(new int[] {1, 2, 3}));
        assertArrayEquals("Testing with {1, 2, 3, 4}", new int[] {1, 4}, kata.makeEnds(new int[] {1, 2, 3, 4}));
        assertArrayEquals("Testing with {7, 4, 6, 2}", new int[] {7, 2}, kata.makeEnds(new int[] {7, 4, 6, 2}));

    }


}
