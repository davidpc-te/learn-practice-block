import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with {4, 5, 6}", new int[] {0, 0, 0, 0, 0, 6}, kata.makeLast(new int[] {4, 5, 6}));
        assertArrayEquals("Testing with {1, 2}", new int[] {0, 0, 0, 2}, kata.makeLast(new int[] {1, 2}));
        assertArrayEquals("Testing with {3}", new int[] {0, 3}, kata.makeLast(new int[] {3}));

    }


}
