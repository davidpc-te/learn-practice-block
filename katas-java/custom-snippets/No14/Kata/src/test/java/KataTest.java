import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {7, 2, 3}", true, kata.no14(new int[] {7, 2, 3}));
        assertEquals("Testing with {1, 2, 3, 4}", false, kata.no14(new int[] {1, 2, 3, 4}));
        assertEquals("Testing with {2, 3, 4}", false, kata.no14(new int[] {2, 3, 4}));

    }


}
