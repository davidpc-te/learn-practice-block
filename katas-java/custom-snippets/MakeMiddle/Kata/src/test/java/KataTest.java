import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with {1, 2, 3, 4}", new int[] {2, 3}, kata.makeMiddle(new int[] {1, 2, 3, 4}));
        assertArrayEquals("Testing with {7, 1, 2, 3, 4, 9}", new int[] {2, 3}, kata.makeMiddle(new int[] {7, 1, 2, 3, 4, 9}));
        assertArrayEquals("Testing with {1, 2}", new int[] {1, 2}, kata.makeMiddle(new int[] {1, 2}));

    }


}
