# Is Strictly Increasing
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 48151e4a-fc02-4cca-9d49-b97376ee3182
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/IsStrictlyIncreasing
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question
 
Create a method called `isStrictlyIncreasing` that takes in an integer array called `nums`. Return true if the values in the array are strictly increasing. Return false otherwise.

For example:
```
isStrictlyIncreasing({5,7,8,10}) → true
isStrictlyIncreasing({5,7,7,10}) → false
isStrictlyIncreasing({-5,-3,0,17}) → true
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public boolean isStrictlyIncreasing(int[] nums) {
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
