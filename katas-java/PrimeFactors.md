# Prime Factors
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: b21a31bd-222e-49db-b7e9-76dd9004a84e
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/PrimeFactors
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `primeFactors` that takes in an integer `n`. Return an integer array of the [prime factors] of `n`(https://www.mathsisfun.com/definitions/prime-factor.html). Prime factors are the numbers you can multiply to get `n` that you can't break down into any smaller factors. You can assume the input is greater than 1.

For example:
```
primeFactors(6) → {2, 3}
primeFactors(28) → {2, 2, 7}
primeFactors(667) → {23, 29}
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int[] primeFactors(int n) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
