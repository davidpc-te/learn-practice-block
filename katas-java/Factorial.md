# Factorial
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 1f69a0aa-4d00-4807-9154-27a5a8997676
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/Factorial
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `factorial` that takes in an integer `n` and returns the factorial of the number. A factorial is the product of all positive integers less than or equal to `n`.

For example:
```
factorial(3) → 6 (since 1 * 2 * 3 = 6)
factorial(4) → 24 (since 1 * 2 * 3 * 4 = 24)
factorial(10) → 3628800
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int factorial(int n) {
    return 0;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
