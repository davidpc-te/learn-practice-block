# List To Array
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 309692e9-826c-4810-b784-9fe2010efdf2
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/List2Array
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `list2Array` that takes in a List of strings called `strings`. Return an array containing the same strings in the same order. Avoid using a standard library method that does the conversion for you.

For example:
```
list2Array( ["aa", "ab", "ac"] ) →   {"aa", "ab", "ac"}
list2Array( ["as", "df", "jk"] ) →   {"as", "df", "jk"}
list2Array( ["aaa", "bbb", "ccc", "ddd"] ) →   {"aaa", "bbb", "ccc", "ddd"}
```
##### !end-question

##### !placeholder


```
import java.util.List;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public String[] list2Array(List<String> strings) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
