# Hello Name
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 6be77e01-5ffa-41f5-be99-852b7e3b1961
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/HelloName
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `helloName` that takes in a string called `name`.
Return a string containing a greeting of the form "Hello ____!" replacing the blank with `name`.

For example:
```
helloName("Bob") → "Hello Bob!"
helloName("Alice") → "Hello Alice!"
helloName("X") → "Hello X!"
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public String helloName(String name) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
